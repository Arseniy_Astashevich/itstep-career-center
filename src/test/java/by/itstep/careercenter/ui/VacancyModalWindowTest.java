package by.itstep.careercenter.ui;

import by.itstep.careercenter.ui.page.MainPageObject;
import by.itstep.careercenter.ui.page.VacancyModalWindow;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VacancyModalWindowTest extends AbstractUiTest{

//    @Test
//    public void open_happyPath() {
//    //given
//    MainPageObject mainPage = new MainPageObject(driver);
//    mainPage.open();
//
//    //when
//    mainPage.openVacancyPopup();
//    VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);
//
//
//    //then
//    Assert.assertEquals();
//
//
//    }

    @Test
    public void readExperience_happyPath() {
        //given
        MainPageObject mainPage = new MainPageObject(driver);
        mainPage.open();
        mainPage.openVacancyPopup();

        //when
        VacancyModalWindow vacancyModalWindow = PageFactory.initElements(driver, VacancyModalWindow.class);

        //then
        Assert.assertNotNull(vacancyModalWindow.readExperience());
    }



}

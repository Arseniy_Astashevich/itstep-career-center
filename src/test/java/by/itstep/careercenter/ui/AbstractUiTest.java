package by.itstep.careercenter.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public abstract class AbstractUiTest {

    protected WebDriver driver;

    @BeforeClass
    public void configureProperty() {
        System.setProperty("webdriver.chrome.driver", "D:/Джава курсы/chromedriver.exe");
    }

    @BeforeMethod
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void shutDown() {
        driver.close();
    }

}

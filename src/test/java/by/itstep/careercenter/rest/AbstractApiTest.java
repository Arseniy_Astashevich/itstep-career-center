package by.itstep.careercenter.rest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeMethod;

import static by.itstep.careercenter.constants.CareerCenterConstants.*;

public abstract class AbstractApiTest {

    @BeforeMethod
    public void setUp() {
        RestAssured.baseURI = HOST;
    }

    protected String getToken() {
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.contentType(ContentType.JSON);
        httpRequest.body("{\n" +
                "  \"password\": \"" + ADMIN_PASSWORD + "\",\n" +
                "  \"username\": \"" + ADMIN_LOGIN + "\"\n" +
                "}");

        return httpRequest.request(Method.POST, "/api/v1/auth/login")
                .body()
                .jsonPath()
                .getString("token");
    }

    protected RequestSpecification getSpecificationWithToken() {
        String t = getToken();

        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Authorization", "Bearer_" + t);
        System.out.println("Some message...");
        return httpRequest;
    }

}

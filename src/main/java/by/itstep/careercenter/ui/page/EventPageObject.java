package by.itstep.careercenter.ui.page;

import by.itstep.careercenter.ui.dto.EventDto;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class EventPageObject extends AbstractPageObject {

    private static final String TAB_TITLE_XPATH = "//div[@class='row section-name']" +
            "//h1";
    private static final String TAB_DESCRIPTION_XPATH = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//div";
    private static final String TAB_DATE_XPATH = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//p[contains(strong,'Время проведения:')]/span";
    private static final String TAB_CATEGORY_XPATH = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//p[contains(strong,'Категория:')]/span";
    private static final String TAB_ADDRESS_XPATH = "//div[@class='row mainContent']//div[@class='col-md-12 col-lg-12']" +
            "//p[contains(strong,'Адрес:')]/span";
    private static final String TAB_REGISTRATION_FORM_XPATH = "//div[@class='buttonWrapper']" +
            "//a[contains(text(),'Записаться')]";




    public EventPageObject(WebDriver driver) {
        super(driver);
    }

    public String readTitle() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_TITLE_XPATH)));

        return tab.getText();
    }

    public String readDescription() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_DESCRIPTION_XPATH)));

        return tab.getText();
    }

    public String readDate() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_DATE_XPATH)));

        return tab.getText();
    }

    public String readCategory() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_CATEGORY_XPATH)));

        return tab.getText();
    }

    public String readAddress() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_ADDRESS_XPATH)));

        return tab.getText();
    }

    public EventDto readEvent () {
        return new EventDto(readTitle(), readDescription(), readDate(), readCategory(), readAddress());
    }

    public void openRegistrationForm() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_REGISTRATION_FORM_XPATH)));

        tab.click();
    }
}

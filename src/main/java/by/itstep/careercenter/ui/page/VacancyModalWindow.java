package by.itstep.careercenter.ui.page;

import by.itstep.careercenter.ui.dto.EventDto;
import by.itstep.careercenter.ui.dto.VacancyDto;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class VacancyModalWindow extends AbstractPageObject{

    @FindBy(xpath = "div[@id='modalVacancy']//div[@class='modal-content']//span[@class='experience']")
    private WebElement experience;

    @FindBy(xpath = "div[@id='modalVacancy']//div[@class='modal-content']//p[@class='company']")
    private WebElement company;

    @FindBy(xpath = "div[@id='modalVacancy']//div[@class='modal-content']//p[@class='position']")
    private WebElement position;

    @FindBy(xpath = "div[@id='modalVacancy']//div[@class='modal-content']//p[@class='description']")
    private WebElement description;

    @FindBy(xpath = "div[@id='modalVacancy']//div[@class='modal-content']//button[@class='close']")
    private WebElement closeButton;

    private By popupLocator = By.className("modalVacancy");


    public VacancyModalWindow(WebDriver driver) {
        super(driver);
    }

    public String readExperience() {
        wait.until(elementToBeClickable(closeButton));
        return experience.getText();
    }

    public String readCompany() {
        return company.getText();
    }

    public String readPosition() {
        return position.getText();
    }

    public String readDescription() {
        return description.getText();
    }

    public VacancyDto readVacancy() {
        return new VacancyDto(readExperience(), readCompany(), readPosition(), readDescription());
    }

    public void closeVacancyPopup_byButton() {
        wait.until(elementToBeClickable(closeButton));
        closeButton.click();
    }

//    public String checkVisibility() {
//        return popupLocator.;
//    }
}

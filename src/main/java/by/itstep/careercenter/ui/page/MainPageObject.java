package by.itstep.careercenter.ui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class MainPageObject extends AbstractPageObject {

    private static final String CAREER_CENTER = "http://localhost:8080/";

    private static final String TAB_EVENT_XPATH = "//div[@class='row events-card-wrapper']//a" +
            "//div[contains(@class, 'events-name')] " +
            "and text() = 'Приглашаем на публичную презентацию курсовых проектов студентов-дизайнеров!'";
    private static final String TAB_ALL_EVENTS_PAGES_XPATH = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть все мероприятия')]";
    private static final String TAB_PROJECT_XPATH = "//div[@class='row main-project']" +
            "//img[@class='image-project-main')]";
    private static final String TAB_ALL_PROJECT_PAGES_XPATH = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть все проекты')]";
    private static final String TAB_STORY_XPATH = "//div[@class='row stories-container']" +
            "//a[@class='storiesModalOpen')]";
    private static final String TAB_ALL_STORIES_PAGE_XPATH = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть еще истории ')]";
    private static final String TAB_VACANCY_POPUP_XPATH = "//div[@class='container-fluid vacancy-wrapper']" +
            "//a[@class='vacancy-box']";
    private static final String TAB_ALL_VACANCIES_PAGE_XPATH = "//div[@class='row see-more-wrapper']" +
            "//a[contains(text(),'Смотреть еще вакансии ')]";
    private static final String TAB_CV_FORM_XPATH = "//div[@id='myFeedback']//div[@class='feedback resume ']" +
            "//a[contains(text(),'Заполнить резюме')]";
    private static final String TAB_GET_CONSULTATION_XPATH = "//div[@id='myFeedback']//div[@class='feedback consult']" +
            "//a[contains(text(),'Получить консультацию')]";


    public MainPageObject(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(CAREER_CENTER);
    }

    public void openEvent() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_EVENT_XPATH)));

        tab.click();
    }

    public void openAllEventsPage() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_ALL_EVENTS_PAGES_XPATH)));

        tab.click();
    }

    public void openProject() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_PROJECT_XPATH)));

        tab.click();
    }

    public void openAllProjectPage() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_ALL_PROJECT_PAGES_XPATH)));

        tab.click();
    }

    public void openStoryPopup() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_STORY_XPATH)));

        tab.click();
    }

    public void openAllStoriesPage() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_ALL_STORIES_PAGE_XPATH)));

        tab.click();
    }

    public VacancyModalWindow openVacancyPopup() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_VACANCY_POPUP_XPATH)));

        tab.click();
        return new VacancyModalWindow(driver);
    }

    public void openAllVacanciesPage() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_ALL_VACANCIES_PAGE_XPATH)));

        tab.click();
    }

    public void openCvFormPage() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_CV_FORM_XPATH)));

        tab.click();
    }

    public void openGetConsultationPopup() {
        WebElement tab = wait
                .until(presenceOfElementLocated(By.xpath(TAB_GET_CONSULTATION_XPATH)));

        tab.click();
    }

}
